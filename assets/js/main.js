console.log("I see you, humanoid.");

$(window).resize(function() {
  if( ($(window).width() / parseFloat($("body").css("font-size"))) <= 41) {
    $("ul.site-nav-list").hide();
    $("a.site-nav-toggle").show();
  }
  else {
    $("ul.site-nav-list").show();
    $("a.site-nav-toggle").hide();
  }
});


$(document).ready(function() {

  /* responsive nav */
  if( ($(window).width() / parseFloat($("body").css("font-size"))) <= 41) {
    $("ul.site-nav-list").hide();
    $("a.site-nav-toggle").show();
  }

  /* menu for smaller viewports */
  $(document).on("click", "#site-nav-toggle", function(e) {
    e.preventDefault();
    if($("ul.site-nav-list").is(":visible")) {
      $("ul.site-nav-list").hide();
    }
    else {
      $("ul.site-nav-list").show();
    }
  });

  /* init jquery appear on panels which are listed in the nav */
  $("div.panel-nav-item").appear();

  /* bind jquery appear to move the menu pill */
  $(document.body).on("appear", ".panel", function(e, $affected) {
    $(".current-page").removeClass("current-page");
    $("a#" + $(this).attr("id") + "-link").addClass("current-page");
  });

  $(document).on("click", "#form-send", function() {
    alert("Form disabled due to security constraints.\n\n Please contact Kevin via Twitter (@kjvarley) or email hello@kevinvarley.uk.");
  });
});
